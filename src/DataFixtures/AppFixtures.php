<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Author;
use App\Entity\Comment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private $articlesTitles = [
        'Awesome Laravel',
        'Keys to knowledge JavaScript',
        'Amazing Go',
        'Admirable Swift',
        'Programming trends of 2020',
        'A quick tour of modern IDEs',
        'Technologies for DevOps 2020',
        'A Complete Guide to Flexbox',
        'A Guide To New And Experimental CSS DevTools In Firefox',
        'How to choose a cloud machine learning platform'
    ];

    private $contents = [
        'Some text',
        'Some content',
        'So...',
        'Ooops.',
        'So, let\'s begin.'
    ];

    private $authorsNames = [
        ['first_name' => 'Martin', 'last_name' => 'Ambroz', 'middle_name' => null],
        ['first_name' => 'Michal', 'last_name' => 'Andrasko', 'middle_name' => null],
        ['first_name' => 'Tomas', 'last_name' => 'Kochevar', 'middle_name' => null],
        ['first_name' => 'Ondra', 'last_name' => 'Kulhanek', 'middle_name' => null],
        ['first_name' => 'Jan', 'last_name' => 'Galuska', 'middle_name' => null],
        ['first_name' => 'Daniel', 'last_name' => 'Brezina', 'middle_name' => null],
        ['first_name' => 'Petr', 'last_name' => 'Dominik', 'middle_name' => null],
        ['first_name' => 'Patrik', 'last_name' => 'Maroucek', 'middle_name' => null],
        ['first_name' => 'Marek', 'last_name' => 'Blatnik', 'middle_name' => null],
        ['first_name' => 'Pavel', 'last_name' => 'Kalivoda', 'middle_name' => null]
    ];

    private $comments = [
        'Thanks! Let\'s more of such articles!',
        'Suitable material',
        'Hey. Very helpful. Do you train in some club?',
        'This is very powerful. The level of me when I was still walking under the table.',
        'Copywriting on the Brazilian system?',
        'Have you already been invited to IBM?',
        'Did you apply for the award right away? Should you nominate article of the month or article of the year?',
        'There is rarely see this... The text is impressive.',
        'Hello. I follow your news. More articles on DevOps from you.',
        'A bit boring'
    ];

    public function load(ObjectManager $manager)
    {
        srand($this->makeSeed());

        $this->seedArticles($manager);

        $this->seedUsers($manager);

        $manager->flush();
    }

    public function seedArticles(ObjectManager $manager)
    {
        for ($aIterator = 0; $aIterator < count($this->articlesTitles); $aIterator++) {
            $article = new Article();
            $article->setHeadline($this->articlesTitles[$aIterator]);
            $article->setContent($this->contents[$aIterator / 2]);
            $article->setPublishedAt($this->getRandomPublishedAt());

            $author = new Author();
            $author->setFirstName($this->authorsNames[$aIterator]['first_name']);
            $author->setLastName($this->authorsNames[$aIterator]['last_name']);
            $author->setMiddleName($this->authorsNames[$aIterator]['last_name']);
            $author->setBirthDate($this->getRandomBirthDate());
            $manager->persist($author);

            $article->addAuthor($author);

            $this->seedComments($manager, $article);
        }
    }

    public function seedComments(ObjectManager $manager, Article $article)
    {
        for ($cIterator = 0; $cIterator < rand(1, 4); $cIterator++) {
            $comment = new Comment();

            $comment->setText($this->comments[rand(0, 9)]);
            $comment->setArticle($article);
            $manager->persist($comment);
        }
        $manager->persist($article);
    }

    public function seedUsers(ObjectManager $manager)
    {
        $user = new User('developer');
        $user->setPassword('Strong#42');
        $user->setEmail('test@test.test');
        $manager->persist($user);
    }

    private function makeSeed()
    {
        list($usec, $sec) = explode(' ', microtime());
        return $sec + $usec * 1000000;
    }

    private function getRandomBirthDate()
    {
        $year = rand(1970, 1995);
        $month = rand(1, 12);
        $day = rand(1, 28);

        return new \DateTime("$year-$month-$day");
    }

    private function getRandomPublishedAt()
    {
        $year = rand(2011, 2019);
        $month = rand(1, 12);
        $day = rand(1, 28);

        return new \DateTime("$year-$month-$day");
    }
}
