1. **Install composer dependencies**

    `composer install`

2. **Deploy the database on your local machine or server**

    `mysql -u <username> -p`
    
    **then:**
    
    `CREATE DATABASE <database name>;`

3. **Update database schema**

    `bin/console doctrine:schema:update --force`

4. **Seed test data to the database**

    `bin/console doctrine:fixtures:load -n`

5. **Start the web server**

    `symfony server:start`

6. **Try to execute request** GET 127.0.0.1:8000/api/articles

    You must to get the response:
    
    `{
        "code": 401,
        "message": "JWT Token not found"
    }`

7. **Try to execute request** POST 127.0.0.1:8000/register

    Pass similar data:
    
    `{
        "username": "developer",
        "password": "Strong#42",
        "email": "goldenarius@gmail.com"
    }`

8. **Try to execute request** GET 127.0.0.1:8000/api/login_check
   
   Pass similar data:
   
   `{
       "username": "developer",
       "password": "Strong#42"
   }`
   
   The answer should be similar to the following:
   
   `{
       "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1OTg0MjgyMjcsImV4cCI6MTU5ODQzMTgyNywicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiZGV2ZWxvcGVyIn0.VmVutNCwV3tLWgVqI6dKzOD6mxcoqoUrfKkk92wj1ZwsB1odCgMX7epHb-_YRZtUWLLzjvPRak-UWVTWWwgBADz-bmZ0cN3ClUy1EfM2SJaQ7fFqNl1n1OPLZTg8VtvZqqrI_8Gg4YznKgJR4--6PfzcaHY74VvNSaIW7No5nP9zcdDZn_piGnoNogw_s5OTBgEYgrWN46unuqzqGoFs9O9soEdnyEn0NkqRZQ7NKHOx1DpAISakTvOTcoZaLVf-kREtN9K7FbolzU56qj-I68_KXxWS_EgBvMv0-67_LbFlZieJf75B_zIUtArLpP1b_wXTVV4U3xKbHMed1KdEUHOWOR5PYcMR1wTlpiuuNYTph3w79D9Vnv2xYse1PH7hRZSczufdNX6zQm9qdcVBXIyrnn2lwgayNrUbBCwfIu-YEh8Pn4KdzkCW_iE6DwY3_250TOHVOb8BnO5B1Zo03y37WicfkkViuqybD2BaQj61uv6LNFuyHVs7UQV6bbPQ0q9KDwASXY-IGB1bX93XN2jOvtpXJuKd_yHNtjpdsYdNy8X5oXjRL2MBaEx74nd3_9P_mUNQH6i-5a-pMI7ppvQ46Y__5QdzSkMVUFvdPTJBjScyUEKf1-AxjXDV62wO-4F57zspaEM-QG2sSmtY9kAdrUgES24oK0eAiFTQEZo"
   }`

9. **Try to execute request** GET 127.0.0.1:8000/api/articles **with authorization header**:
    
    **Authorization**   _Bearer \<token\>_
   
   You must to get the response like this:
   
   `{
       "@context": "/api/contexts/Article",
       "@id": "/api/articles",
       "@type": "hydra:Collection",
       "hydra:member": [
           {
               "@id": "/api/articles/5c9fd3fb-e775-11ea-93fd-9822ef6745a1",
               "@type": "Article",
               "id": "5c9fd3fb-e775-11ea-93fd-9822ef6745a1",
               "comments": [
                   "/api/comments/5c9fcf50-e775-11ea-93fd-9822ef6745a1"
               ],
               "headline": "Keys to knowledge JavaScript",
               "content": "Some text",
               "publishedAt": "2016-12-05T00:00:00+00:00",
               "authors": [
                   "/api/authors/5c9fc84b-e775-11ea-93fd-9822ef6745a1"
               ],
               "createdAt": "2020-08-26T08:23:03+00:00",
               "updatedAt": "2020-08-26T08:23:03+00:00"
           },
           ...
       ]
   }`